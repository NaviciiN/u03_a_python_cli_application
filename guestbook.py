import json
from datetime import datetime
import argparse

def load_entries(filename):
    try:
        with open(filename) as f:
            entries = json.load(f)
    except FileNotFoundError:
        entries = []
    return entries

def save_entries(filename, entries):
    with open(filename, "w") as f:
        json.dump(entries, f)

def add_entry(filename, entry):
    entries = load_entries(filename)
    entries.append(entry)
    save_entries(filename, entries)

def edit_entry(filename, index, entry):
    entries = load_entries(filename)
    # Check if there are at least 3 entries in the guestbook
    if len(entries) >= 3:
        # Update the content of the third note
        entries[-index]['content'] = (" ".join(entry))
        save_entries(filename, entries)
        print("Entry edited successfully.")
    else:
        print("There are fewer than three entries in the guestbook.")




def delete_entry(filename, index):
    entries = load_entries(filename)
    if index >= len(entries):
        print("This index does not exist currently!")
        return
    if index <= len(entries):
     del entries[index]
     print("Entry deleted successfully.")
    save_entries(filename, entries)

def display_entries(filename):
    entries = load_entries(filename)
    for entry in entries:
        print(f"{entry['timestamp']}: {entry['content']}")

def export_guestbook_entries(filename):
    # Load the guestbook entries from the file
    entries = load_entries(filename)

    # Print the entries in JSON format
    print(json.dumps(entries, indent=4))

def add_guestbook_entry(filename, entry):
    # Create a new entry dictionary
    new_entry = {"content": entry, "timestamp": datetime.now().isoformat()}

    # Add the new entry to the file
    add_entry(filename, new_entry)

    print("Entry added successfully.")

def edit_guestbook_entry(filename, index, entry):
    # Edit the entry in the file
    edit_entry(filename, index, entry)
    

def delete_guestbook_entry(filename, index):
    # Delete the entry from the file
    delete_entry(filename, index)

    

def main():
    parser = argparse.ArgumentParser(description='Guestbook program')
    parser.add_argument('command', choices=['new', 'list', 'edit', 'delete', 'export'], help='Command to execute')
    parser.add_argument('args', nargs='*', help='Arguments for the command')

    args = parser.parse_args()

    filename = "guestbook.json"

    if args.command == "new":
        if not args.args:
            print("Please provide the content of the new note")
            return
        content = " ".join(args.args)
        add_guestbook_entry(filename, content)

    elif args.command == "list":
        display_entries(filename)

    elif args.command == "edit":
        if len(args.args) != 2:
            print("Please provide the index of the note to edit and the new content")
            return
        #elif index != list:
        #  print("Please enter an available index!")
        index = int(args.args[0])
        content = args.args[1]
        edit_guestbook_entry(filename, index, content)

    elif args.command == "delete":
        if not args.args:
            print("Please provide the index of the note to delete")
            return
        index = int(args.args[0])
        delete_guestbook_entry(filename, index)

    elif args.command == "export":
        export_guestbook_entries(filename)

if __name__ == "__main__":
    main()
