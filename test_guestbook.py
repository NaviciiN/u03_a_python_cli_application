import os
import json
import pytest
from guestbook import load_entries, save_entries, add_entry, edit_entry, delete_entry, display_entries, export_guestbook_entries


@pytest.fixture
def entries_file(tmp_path):
    return tmp_path / "entries.json"


def test_load_entries(entries_file):
    # Test loading entries from an empty file
    assert load_entries(entries_file) == []

    # Test loading entries from a non-empty file
    entries = [{"content": "entry 1", "timestamp": "2022-04-13T14:30:00.000000"}, {"content": "entry 2", "timestamp": "2022-04-14T14:30:00.000000"}]
    with open(entries_file, "w") as f:
        json.dump(entries, f)
    assert load_entries(entries_file) == entries


def test_save_entries(entries_file):
    # Test saving entries to a file
    entries = [{"content": "entry 1", "timestamp": "2022-04-13T14:30:00.000000"}, {"content": "entry 2", "timestamp": "2022-04-14T14:30:00.000000"}]
    save_entries(entries_file, entries)
    with open(entries_file) as f:
        assert json.load(f) == entries


def test_add_entry(entries_file):
    # Test adding a new entry
    entry = {"content": "new entry", "timestamp": "2022-04-15T14:30:00.000000"}
    add_entry(entries_file, entry)
    assert load_entries(entries_file) == [entry]


def test_edit_entry(entries_file):
    # Test editing an existing entry
    entries = [{"content": "entry 1", "timestamp": "2022-04-13T14:30:00.000000"}, {"content": "entry 2", "timestamp": "2022-04-14T14:30:00.000000"}]
    save_entries(entries_file, entries)
    edited_entry = {"content": "edited entry", "timestamp": "2022-04-13T14:30:00.000000"}
    edit_entry(entries_file, 0, edited_entry["content"])
    entries[0] = edited_entry
    with open(entries_file) as f:
        assert json.load(f) == entries


def test_delete_entry(entries_file):
    # Test deleting an existing entry
    entries = [{"content": "entry 1", "timestamp": "2022-04-13T14:30:00.000000"}, {"content": "entry 2", "timestamp": "2022-04-14T14:30:00.000000"}]
    save_entries(entries_file, entries)
    delete_entry(entries_file, 1)
    del entries[1]
    with open(entries_file) as f:
        assert json.load(f) == entries


def test_display_entries(entries_file, capsys):
    # Test displaying all entries
    entries = [{"content": "entry 1", "timestamp": "2022-04-13T14:30:00.000000"}, {"content": "entry 2", "timestamp": "2022-04-14T14:30:00.000000"}]
    save_entries(entries_file, entries)
    display_entries(entries_file)
    captured = capsys.readouterr()
    assert captured.out.strip() == "2022-04-13T14:30:00.000000: entry 1\n2022-04-14T14:30:00.000000: entry 2"
